//wrapper
const formWrapper = document.getElementsByClassName("signup-form-wrapper")[0];
//buttom
const formButtom = document.getElementsByClassName("signup-form-buttom")[0];
//input
const passwordInput = document.signUpForm.signUpPassword;
//inputs values
const emailValue = document.signUpForm.signUpEmail;
const passwordValue = document.signUpForm.signUpPassword;
const passwordConfirmationValue = document.signUpForm.signUpPasswordConfirmation;
//svg
const svgLogo = document.getElementsByClassName("signup-img")[0];
//style manipulation
const weakPassword = document.getElementsByClassName("signup-form-validation-item")[0].style;
const mediumPassword = document.getElementsByClassName("signup-form-validation-item")[1].style;
const strongPassword = document.getElementsByClassName("signup-form-validation-item")[2].style;
const lengthPasswordFilter =document.getElementsByClassName("signup-form-validation-circle")[0].style;
const capsPasswordFilter =document.getElementsByClassName("signup-form-validation-circle")[1].style;
const numberPasswordFilter =document.getElementsByClassName("signup-form-validation-circle")[2].style;
const welcomeSignUp = document.getElementsByClassName("signup-welcome")[0];
//regular expressions
const hasNumberPassword = /\d/;
const hasCapsPassword = /[A-Z]/;
const isEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

//form contructor
class Form {
	constructor(email,password,passwordConfirmation) {
		this._email = email;
		this._password = password;
		this._passwordConfirmation = passwordConfirmation;
	}
	//validation before submit the form
	preValidation() {
	var countValidation = 0;
	//Password filters tests
		this._password.value.length >= 6 ? (lengthPasswordFilter.color = "#1ED699") && countValidation++ : lengthPasswordFilter.color = "#F26722";
		hasCapsPassword.test(this._password.value) == true ? (capsPasswordFilter.color = "#1ED699")  && countValidation++ : capsPasswordFilter.color = "#F26722";
		hasNumberPassword.test(this._password.value) == true ? (numberPasswordFilter.color = "#1ED699")  && countValidation++ : numberPasswordFilter.color = "#F26722";
	//Number of validation tests that has been approved
		switch (countValidation) {
			case 0:
			 	mediumPassword.backgroundColor = "#EDEDED";
		        weakPassword.backgroundColor = "#EDEDED";
				strongPassword.backgroundColor = "#EDEDED";
				passwordInput.style.borderColor = "#EDEDED";
				break;
    		case 1:
		        weakPassword.backgroundColor = "#F26722";
		        mediumPassword.backgroundColor = "#EDEDED";
				strongPassword.backgroundColor = "#EDEDED";
				passwordInput.style.borderColor = "#F26722";
		        break;
    		case 2:
		        mediumPassword.backgroundColor = "#F2B822";
		        weakPassword.backgroundColor = "#F2B822";
		        strongPassword.backgroundColor = "#EDEDED";
		        passwordInput.style.borderColor = "#F2B822";
		        break;
 			case 3:
 				mediumPassword.backgroundColor = "#1ED699";
		        weakPassword.backgroundColor = "#1ED699";
				strongPassword.backgroundColor = "#1ED699";
				passwordInput.style.borderColor = "#1ED699";
				break;
		}
	//Check if input is empty		
		this._password.value.length == 0 ? (lengthPasswordFilter.color = "#EDEDED") && (capsPasswordFilter.color = "#EDEDED") && (numberPasswordFilter.color = "#EDEDED") : null;	
	}
	//validation after click in the buttom
	validation() {
		//validation password confirmation
		(this._password == this._passwordConfirmation) && (countValidation == 3) ? null : (passwordConfirmationValue.style.borderColor = "#F26722") && (passwordValue.style.borderColor = "#F26722");
		//validate if is an existent email
		isEmail.test((String(this._email.value).toLowerCase())) ? null : emailValue.style.borderColor = "#F26722";
		setTimeout(() => {
				emailValue.style.borderColor = "#BFBFBF";
				passwordValue.style.borderColor = "#BFBFBF";
				passwordConfirmationValue.style.borderColor = "#BFBFBF";
			}, 500);
		//animation after sucess 
		svgLogo.style.animationName = "hide";
			setTimeout(() => {
				svgLogo.style.opacity = "0";
				svgLogo.src = "logoPolvoWatch.svg";
				svgLogo.style.animationName = "show";
			}, 1600);
		welcomeSignUp.innerHTML = "Cadastrando..";
		formWrapper.classList.add("signup-hidden-mensage");
	}
}
//new object form
const signUpForm = new Form(emailValue,passwordValue,passwordConfirmationValue);

//main
passwordInput.addEventListener ("keyup", ()=> {
	signUpForm.preValidation();
});
formButtom.addEventListener ("click", (event)=> {
	event.preventDefault();
	signUpForm.validation();
});
